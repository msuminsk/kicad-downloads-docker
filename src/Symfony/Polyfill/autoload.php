<?php
// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
require_once '/usr/share/php/Fedora/Autoloader'.'/autoload.php';

\Fedora\Autoloader\Autoload::addClassMap(
    array(
        'arithmeticerror' => '/Php70/Resources/stubs/ArithmeticError.php',
                'assertionerror' => '/Php70/Resources/stubs/AssertionError.php',
                'callbackfilteriterator' => '/Php54/Resources/stubs/CallbackFilterIterator.php',
                'divisionbyzeroerror' => '/Php70/Resources/stubs/DivisionByZeroError.php',
                'error' => '/Php70/Resources/stubs/Error.php',
                'parseerror' => '/Php70/Resources/stubs/ParseError.php',
                'recursivecallbackfilteriterator' => '/Php54/Resources/stubs/RecursiveCallbackFilterIterator.php',
                'sessionhandlerinterface' => '/Php54/Resources/stubs/SessionHandlerInterface.php',
                'symfony\\polyfill\\php54\\php54' => '/Php54/Php54.php',
                'symfony\\polyfill\\php55\\php55' => '/Php55/Php55.php',
                'symfony\\polyfill\\php55\\php55arraycolumn' => '/Php55/Php55ArrayColumn.php',
                'symfony\\polyfill\\php56\\php56' => '/Php56/Php56.php',
                'symfony\\polyfill\\php70\\php70' => '/Php70/Php70.php',
                'symfony\\polyfill\\php71\\php71' => '/Php71/Php71.php',
                'symfony\\polyfill\\php72\\php72' => '/Php72/Php72.php',
                'symfony\\polyfill\\util\\binary' => '/Util/Binary.php',
                'symfony\\polyfill\\util\\binarynofuncoverload' => '/Util/BinaryNoFuncOverload.php',
                'symfony\\polyfill\\util\\binaryonfuncoverload' => '/Util/BinaryOnFuncOverload.php',
                'symfony\\polyfill\\util\\testlistener' => '/Util/TestListener.php',
                'typeerror' => '/Php70/Resources/stubs/TypeError.php',
    ),
    __DIR__
);
// @codeCoverageIgnoreEnd

\Fedora\Autoloader\Dependencies::required(array(
    __DIR__ . '/Php54/bootstrap.php',
    __DIR__ . '/Php55/bootstrap.php',
    __DIR__ . '/Php56/bootstrap.php',
    __DIR__ . '/Php70/bootstrap.php',
    __DIR__ . '/Php71/bootstrap.php',
    __DIR__ . '/Php72/bootstrap.php',
    '/usr/share/php/password_compat/password.php',
    '/usr/share/php/random_compat/autoload.php',
));
