<?php
/**
 * Autoloader for all Symfony bridges/bundles/components and their dependencies.
 *
 * Created by php-symfony-2.8.12-2.el7
 *
 * Note: This autoloader does not do any registration with the Doctrine
 * annotation registry (i.e. \Doctrine\Common\Annotations\AnnotationRegistry).
 *
 * @return \Symfony\Component\ClassLoader\ClassLoader
 */

require_once __DIR__ . '/Component/autoload.php';

$fedoraClassLoader->addPrefix('Symfony\\', dirname(__DIR__));

// Dependency autoloaders
foreach (array(
    '/usr/share/php/Doctrine/Bundle/DoctrineBundle/autoload.php',
    '/usr/share/php/Doctrine/Common/Annotations/autoload.php',
    '/usr/share/php/Doctrine/Common/autoload.php',
    '/usr/share/php/Doctrine/Common/Cache/autoload.php',
    '/usr/share/php/Doctrine/Common/DataFixtures/autoload.php',
    '/usr/share/php/Doctrine/DBAL/autoload.php',
    '/usr/share/php/Doctrine/ORM/autoload.php',
    '/usr/share/php/Egulias/EmailValidator/autoload.php',
    '/usr/share/php/Monolog/autoload.php',
    '/usr/share/php/phpDocumentor/Reflection/autoload.php',
    '/usr/share/php/ProxyManager/autoload.php',
    '/usr/share/php/Psr/Log/autoload.php',
    '/usr/share/php/Swift/swift_required.php',
    '/usr/share/php/Symfony/Polyfill/autoload.php',
    '/usr/share/php/Symfony/Security/Acl/autoload.php',
    '/usr/share/php/Twig/autoload.php',
) as $dependencyAutoloader) {
    if (file_exists($dependencyAutoloader)) {
        require_once $dependencyAutoloader;
    }
}

return $fedoraClassLoader;
