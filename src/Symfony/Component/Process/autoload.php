<?php
/**
 * Autoloader for all Symfony components and their dependencies.
 *
 * Created by php-symfony-2.8.12-2.el7
 *
 * @return \Symfony\Component\ClassLoader\ClassLoader
 */

if (!isset($fedoraClassLoader) || !($fedoraClassLoader instanceof \Symfony\Component\ClassLoader\ClassLoader)) {
    if (!class_exists('Symfony\\Component\\ClassLoader\\ClassLoader', false)) {
        require_once __DIR__ . '/ClassLoader/ClassLoader.php';
    }

    $fedoraClassLoader = new \Symfony\Component\ClassLoader\ClassLoader();
    $fedoraClassLoader->register();
}
$fedoraClassLoader->addPrefix('Symfony\\Component\\', dirname(dirname(__DIR__)));

// Optional dependency
foreach (array(
    '/usr/share/php/random_compat/autoload.php',
) as $dependencyAutoloader) {
    if (file_exists($dependencyAutoloader)) {
        require_once $dependencyAutoloader;
    }
}

return $fedoraClassLoader;
