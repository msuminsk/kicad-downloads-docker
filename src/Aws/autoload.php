<?php
/**
 * Autoloader for php-aws-sdk and its' dependencies
 * (created by php-aws-sdk-2.8.31-1.el7).
 *
 * @return \Symfony\Component\ClassLoader\ClassLoader
 */

if (!isset($fedoraClassLoader) || !($fedoraClassLoader instanceof \Symfony\Component\ClassLoader\ClassLoader)) {
    if (!class_exists('Symfony\\Component\\ClassLoader\\ClassLoader', false)) {
        require_once '/var/www/src/Symfony/Component/ClassLoader/ClassLoader.php';
    }

    $fedoraClassLoader = new \Symfony\Component\ClassLoader\ClassLoader();
    $fedoraClassLoader->register();
}

$fedoraClassLoader->addPrefix('Aws\\', dirname(__DIR__));

// Dependencies (autoloader => required)
foreach(array(
    // Required dependencies
    '../Guzzle/autoload.php'                 => true,
    // Optional dependencies
    '/usr/share/php/Doctrine/Common/Cache/autoload.php'  => false,
    '/usr/share/php/Monolog/autoload.php'                => false,
    '/var/www/src/Symfony/Component/Yaml/autoload.php' => false,
) as $dependencyAutoloader => $required) {
    if ($required || file_exists($dependencyAutoloader)) {
        require_once $dependencyAutoloader;
    }
}

return $fedoraClassLoader;
