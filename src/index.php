<html>
<head>
<title>KiCad downloads page</title>
</head>
<body>
<?php
// Configuration variables
$bucketName = 'kicad-downloads';
$endpoint = 'https://s3.cern.ch';
$downloadUrl = '/p/';


require('Aws/autoload.php');
use Aws\S3\S3Client;

$startTime = microtime(true);
$path = isset($_GET['p']) ? filter_var($_GET['p'], FILTER_SANITIZE_STRING) : '.';
$is_root_path = ($path == '.');

$client = S3Client::factory(array(
	'endpoint' => $endpoint,
	'credentials' => false
));

$objects = $client->ListObjects([
	'Bucket' => $bucketName,
	'delimiter'=> '/',
]);


$files = array();
$dirs = array();

// Prepare the list of files and directories
foreach ($objects['Contents'] as $entry => $v) {
	$full_path = $v['Key'];

	// Display only entries in the selected directory
	if (!$is_root_path && dirname($full_path) != $path)
		continue;

	$rel_path = $is_root_path ? $full_path : str_replace($path, '', $full_path);
	$rel_path_exploded = explode('/', ltrim($rel_path, '/'));
	$is_dir = count($rel_path_exploded) > 1;

	if ($is_dir) {
		// Display only files in the selected directory
		$dir_name = $rel_path_exploded[0];

		if (key_exists($dir_name, $dirs))
			continue;

		$link_path = $is_root_path ? "${rel_path_exploded[0]}" : "${path}/${rel_path_exploded[0]}";
		$data['link'] = "?p=${link_path}";
		//$data['last_mod'] = '';	// not displayed for directories
		//$data['size'] = '';

		$dirs[$dir_name] = $data;
	} else {
		$file_name = array_slice($rel_path_exploded, -1)[0];

		$data['link'] = "${downloadUrl}${full_path}";
		$data['last_mod'] = $v['LastModified'];
		$data['size'] = $v['Size'];

		$files[$file_name] = $data;
	}
}


// Generate the page
print("<h1>Index of ${path}</h1>");
print("<table width='100%'>");

// Display '..' to return to the previous directory
if (!$is_root_path) {
	$slash_pos = strrpos($path, '/');

	if($slash_pos == 0)		// first level directory returns to the root
		$up_dir = '.';
	else
		$up_dir = substr($path, 0, $slash_pos);

	print("<tr><td colspan='3'><a href='?p=${up_dir}'><b>../</b></a></td></tr>");
}


// List of directories
foreach ($dirs as $dir_name => $data) {
	print("<tr>");
	print("<td colspan='3'><a href='${data['link']}'><b>${dir_name}/</b></a></td>");
	print("</tr>");
}

// List of files
foreach ($files as $file_name => $data) {
	print("<tr>");
	print("<td><a href='${data['link']}'>${file_name}</a></td>");
	print("<td>${data['last_mod']}</td>");
	print("<td>${data['size']}</td>");
	print("</tr>");
}

print("</table>");


// Profiling
$finishTime = microtime(true);
$executionTime = $finishTime - $startTime;
print("<hr /><small><i>Generated in ${executionTime} microseconds</i></small>");
?>
</body>
</html>
