<?php
/**
 * Autoloader for php-guzzle-Guzzle and its' dependencies
 * (created by php-guzzle-Guzzle-3.9.3-11.el7).
 */
require_once '/usr/share/php/Fedora/Autoloader/autoload.php';

\Fedora\Autoloader\Autoload::addPsr4('Guzzle\\', __DIR__);

\Fedora\Autoloader\Dependencies::required(array(
    '/usr/share/php/Symfony/Component/EventDispatcher/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
    '/usr/share/php/Doctrine/Common/Cache/autoload.php',
    '/usr/share/php/Monolog/autoload.php',
    '/usr/share/php/Zend/autoload.php',
));
